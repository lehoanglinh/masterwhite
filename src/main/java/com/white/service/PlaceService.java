
package com.white.service;

import java.util.List;

import com.white.model.Place;

public interface PlaceService {

	public void addPlace(Place p);

	public void updatePlace(Place p);

	public List<Place> listPlaces();

	public Place getPlaceById(int id);

	public void removePlace(int id);

}
