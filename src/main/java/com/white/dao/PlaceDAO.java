package com.white.dao;

import java.util.List;

import com.white.model.Place;

public interface PlaceDAO {
//	public void save(Place p);
//
//	public List<Place> list();
	public void addPlace(Place p);

	public void updatePlace(Place p);

	public List<Place> listPlaces();

	public Place getPlaceById(int id);

	public void removePlace(int id);
}
