
package com.white.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.white.model.Place;
import com.white.service.PlaceService;

@Controller
public class PlaceController {
	
	private PlaceService placeService;
	
	@Autowired(required=true)
	@Qualifier(value="placeService")
	public void setPlaceService(PlaceService ps){
		this.placeService = ps;
	}
	
	@RequestMapping(value = "/places", method = RequestMethod.GET)
	public String listplaces(Model model) {
		model.addAttribute("place", new Place());
		model.addAttribute("listplaces", this.placeService.listPlaces());
		return "place";
	}
	
	//For add and update place both
	@RequestMapping(value= "/place/add", method = RequestMethod.POST)
	public String addplace(@ModelAttribute("place") Place p){
		
		if(p.getId() == 0){
			//new place, add it
			this.placeService.addPlace(p);
		}else{
			//existing place, call update
			this.placeService.updatePlace(p);
		}
		
		return "redirect:/places";
		
	}
	
	@RequestMapping("/remove/{id}")
    public String removeplace(@PathVariable("id") int id){
		
        this.placeService.removePlace(id);
        return "redirect:/places";
    }
 
    @RequestMapping("/edit/{id}")
    public String editplace(@PathVariable("id") int id, Model model){
        model.addAttribute("place", this.placeService.getPlaceById(id));
        model.addAttribute("listplaces", this.placeService.listPlaces());
        return "place";
    }
	
}
